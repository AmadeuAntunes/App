﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace projecto01.Droid
{
	[Activity (Label = "projecto01.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{

        private EditText numero1, numero2;
        private Button btn01;
        private TextView resultado;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            SetContentView(Resource.Layout.layout01);
            numero1 = FindViewById<EditText> (Resource.Id.txtNumero1);
            numero2 = FindViewById<EditText>(Resource.Id.txtNumero2);
            btn01 = FindViewById<Button>(Resource.Id.btn01);
            resultado = FindViewById<TextView>(Resource.Id.resultado);

            btn01.Click += delegate
            {
                int num1 = int.Parse(numero1.Text);
                int num2 = int.Parse(numero2.Text);
                int result = MyClass.soma(num1, num2);

                resultado.Text = "A soma dos valores é " + result;
            };

        }

	}
}


